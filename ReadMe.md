# Project Name

> Short description about the project.

![][badge]


Descript what this is, and what it does. Span this over a couple of paragraphs.

![][project_image]


## Installation

###### Configure

```bash
./configure --with-example="this"
make -j5
sudo make install
```

###### Install

```bash
sudo install -v -m 0755 example_script /usr/local/bin
```


## Usage example

```bash
$ example_script -o bar /path/to/some/dir
```

_For more examples and usage, please refer to the [Wiki][wiki]._


## Release History

-   v0.5.0
    * UPDATE: Man page
-   0.4.0
    * FIX: `more()`
-   v0.3.0
    * NEW: More options
-   v0.2.0
    * FIX: set `this()` to `that()`
-   v0.1.0
    * Initial release


## License

Distributed under the XYZ license. See [`LICENSE`][licens] for more information.


## Meta

Email: First Lastname &lt;first.lastname@domain.tld&gt;

Twitter: [@Username][twitter]

Gitlab: <https://gitlab.com/username/project>


## Contributing

1. Fork it (<https://gitlab.com/username/project/forks/new>)
2. Create your feature branch (`git checkout -b feature/this`)
3. Commit your changes (`git commit -am 'Add this feature'`)
4. Push to the branch (`git push origin feature/this`)
5. Create a new Merge Request


<!-- Markdown link & img dfn's -->
[project_image]: files/project.jpg "Project Sample Image"
[badge]: https://img.shields.io/badge/project-v1.0.0-778899.svg?style=plastic "Version"
[wiki]: https://gitlab.com/username/project/-/wikis/home "Project :: Wiki"
[licens]: LICENSE "XYZ License"
[twitter]: # "Username @ Twitter"
